USE [master]
GO
/****** Object:  Database [TrackX]    Script Date: 06-10-2021 09:11:02 ******/
CREATE DATABASE [TrackX]
GO

USE [TrackX]
GO

CREATE TABLE [dbo].[customers](
	[id] [int] primary key IDENTITY(1,1) NOT NULL ,
	[name] [varchar](50) NOT NULL,
	[phonenumber] [varchar](10) NULL,)
GO

CREATE TABLE [dbo].[locations](
	[id] [int] primary key IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	)
GO

CREATE TABLE [dbo].[drivers](
	[id] [int] primary key IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[phonenumber] [varchar](10) NULL,
	[vehiclenumber] [varchar](20) NULL,
	[place] [int]   foreign key references locations(id) ),
GO


CREATE TABLE [dbo].[source_destination](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[source] [int] NULL foreign key references locations(id),
	[destination] [int] NULL foreign key references locations(id),
	[distance] [int] NOT NULL,
	)
GO


CREATE TABLE [dbo].[rides](
	[id] [int]  primary key IDENTITY(1,1) NOT NULL,
	[cid] [int] NULL foreign key references locations(id),
	[did] [int] NULL foreign key references locations(id),
	[sdid] [int] NULL foreign key references source_destination(id) ,
	[price] [int] NOT NULL,
	[date] [date] NOT NULL,
	[status] [varchar](50) NOT NULL,)
GO

INSERT [dbo].[customers] ([id], [name], [phonenumber]) VALUES (1, N'rajesh', N'8247364226')
GO
INSERT [dbo].[customers] ([id], [name], [phonenumber]) VALUES (2, N'kamal', N'9867054321')
GO
INSERT [dbo].[customers] ([id], [name], [phonenumber]) VALUES (3, N'ram', N'897654321')
GO
INSERT [dbo].[customers] ([id], [name], [phonenumber]) VALUES (4, N'srinu', N'985674321')
GO
INSERT [dbo].[customers] ([id], [name], [phonenumber]) VALUES (5, N'arvind', N'789654321')
GO
INSERT [dbo].[customers] ([id], [name], [phonenumber]) VALUES (6, N'rajeev', N'7894561230')
GO

INSERT [dbo].[drivers] ([id], [name], [phonenumber], [vehiclenumber], [place]) VALUES (1, N'rahul', N'989898989', N'ap12ts123', 1)
GO
INSERT [dbo].[drivers] ([id], [name], [phonenumber], [vehiclenumber], [place]) VALUES (2, N'lucky', N'98987654', N'ap2a123', 1)
GO
INSERT [dbo].[drivers] ([id], [name], [phonenumber], [vehiclenumber], [place]) VALUES (3, N'ram', N'700098098', N'ap12x457', 2)
GO
INSERT [dbo].[drivers] ([id], [name], [phonenumber], [vehiclenumber], [place]) VALUES (4, N'kalyan', N'87236728', N'khadk', 3)
GO
INSERT [dbo].[drivers] ([id], [name], [phonenumber], [vehiclenumber], [place]) VALUES (5, N'ramesh', N'8038832', N'kshdsklj', 4)
GO
INSERT [dbo].[drivers] ([id], [name], [phonenumber], [vehiclenumber], [place]) VALUES (8, N'tarun', N'9278927', N'ayssdhj', 5)
GO
INSERT [dbo].[drivers] ([id], [name], [phonenumber], [vehiclenumber], [place]) VALUES (9, N'rex', N'808080809', N'kshahksba', 4)
GO
INSERT [dbo].[drivers] ([id], [name], [phonenumber], [vehiclenumber], [place]) VALUES (10, N'rahim', N'987989', N'kshks', 10)
GO
INSERT [dbo].[drivers] ([id], [name], [phonenumber], [vehiclenumber], [place]) VALUES (12, N'suresh', N'797870870', N'8779797', 9)
GO
INSERT [dbo].[drivers] ([id], [name], [phonenumber], [vehiclenumber], [place]) VALUES (13, N'harish', N'98873249', N'ap12ts123', 10)
GO
INSERT [dbo].[drivers] ([id], [name], [phonenumber], [vehiclenumber], [place]) VALUES (14, N'madhan', N'9848743984', N'ap12ts123', 7)
GO
INSERT [dbo].[drivers] ([id], [name], [phonenumber], [vehiclenumber], [place]) VALUES (15, N'ravi', N'8576534242', N'ap12ts123', 6)
GO
INSERT [dbo].[drivers] ([id], [name], [phonenumber], [vehiclenumber], [place]) VALUES (16, N'rambabu', N'7546454378', N'ap12ts123', 7)
GO

INSERT [dbo].[locations] ([id], [name]) VALUES (1, N'gandhinagar')
GO
INSERT [dbo].[locations] ([id], [name]) VALUES (2, N'mg road')
GO
INSERT [dbo].[locations] ([id], [name]) VALUES (3, N'besant road')
GO
INSERT [dbo].[locations] ([id], [name]) VALUES (4, N'pvr mall')
GO
INSERT [dbo].[locations] ([id], [name]) VALUES (5, N'benz circle')
GO
INSERT [dbo].[locations] ([id], [name]) VALUES (6, N'ntr circle')
GO
INSERT [dbo].[locations] ([id], [name]) VALUES (7, N'railway station')
GO
INSERT [dbo].[locations] ([id], [name]) VALUES (8, N'kr market')
GO
INSERT [dbo].[locations] ([id], [name]) VALUES (9, N'bsnl office')
GO
INSERT [dbo].[locations] ([id], [name]) VALUES (10, N'busstand')
GO

INSERT [dbo].[rides] ([id], [cid], [did], [sdid], [price], [date], [status]) VALUES (6, 1, 1, 3, 456, CAST(N'1999-08-17' AS Date), N'completed')
GO
INSERT [dbo].[rides] ([id], [cid], [did], [sdid], [price], [date], [status]) VALUES (7, 2, 2, 4, 12, CAST(N'1999-08-16' AS Date), N'completed')
GO
INSERT [dbo].[rides] ([id], [cid], [did], [sdid], [price], [date], [status]) VALUES (8, 3, 5, 3, 56, CAST(N'1999-08-19' AS Date), N'completed')
GO
INSERT [dbo].[rides] ([id], [cid], [did], [sdid], [price], [date], [status]) VALUES (9, 4, 5, 3, 56, CAST(N'1999-08-12' AS Date), N'started')
GO
INSERT [dbo].[rides] ([id], [cid], [did], [sdid], [price], [date], [status]) VALUES (10, 1, 3, 7, 100, CAST(N'1999-08-17' AS Date), N'pending')
GO
INSERT [dbo].[rides] ([id], [cid], [did], [sdid], [price], [date], [status]) VALUES (11, 1, 3, 7, 100, CAST(N'1999-08-17' AS Date), N'pending')
GO
INSERT [dbo].[rides] ([id], [cid], [did], [sdid], [price], [date], [status]) VALUES (12, 1, 3, 7, 100, CAST(N'1999-08-17' AS Date), N'pending')
GO
SET IDENTITY_INSERT [dbo].[rides] OFF
GO
SET IDENTITY_INSERT [dbo].[source_destination] ON 
GO
INSERT [dbo].[source_destination] ([id], [source], [destination], [distance]) VALUES (3, 1, 2, 2)
GO
INSERT [dbo].[source_destination] ([id], [source], [destination], [distance]) VALUES (4, 3, 2, 5)
GO
INSERT [dbo].[source_destination] ([id], [source], [destination], [distance]) VALUES (5, 10, 7, 5)
GO
INSERT [dbo].[source_destination] ([id], [source], [destination], [distance]) VALUES (6, 5, 4, 4)
GO
INSERT [dbo].[source_destination] ([id], [source], [destination], [distance]) VALUES (7, 4, 3, 6)
GO
INSERT [dbo].[source_destination] ([id], [source], [destination], [distance]) VALUES (8, 1, 3, 6)
GO
INSERT [dbo].[source_destination] ([id], [source], [destination], [distance]) VALUES (9, 1, 4, 3)
GO
INSERT [dbo].[source_destination] ([id], [source], [destination], [distance]) VALUES (10, 2, 3, 4)
GO
INSERT [dbo].[source_destination] ([id], [source], [destination], [distance]) VALUES (11, 6, 5, 2)
GO
INSERT [dbo].[source_destination] ([id], [source], [destination], [distance]) VALUES (12, 6, 4, 5)
GO


