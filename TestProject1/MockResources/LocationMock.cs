﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using TrackX.Models.DB;
using TrackX.Repository.IRepository;

namespace TestProject1.MockResources
{
    public class LocationMock
    {
        public static readonly Mock<ILocationRepository> LocationRepoMock = new Mock<ILocationRepository>();
        public static IEnumerable<Location> locations = new List<Location>{
            new Location{id=1,name="gandhinagar"},
             new Location{id=2,name= "mg road"},
             new Location{id =3,name ="besant road"},
             new Location{id=4,name ="pvr mall"},
             
        };

        public static List<Sourcedestination> sourcedestinations = new List<Sourcedestination>
        {
            new Sourcedestination{id=1,source=1,destination=2,distance=3 },
            new Sourcedestination{id=2,source=1,destination=3,distance=4 },
            new Sourcedestination{id=3,source=3,destination=2,distance=6 },
            new Sourcedestination{id=4,source=4,destination=3,distance=7 },
            new Sourcedestination{id=5,source=1,destination=4,distance=6 },
            new Sourcedestination{id=6,source=4,destination=2,distance=3 },
        };

        public static List<Ride> rides = new List<Ride>
        {
            new Ride{id=1,cid=1,did=1,sdid=1,price=100,date=new DateTime(2016, 02, 16),status="completed" },
            new Ride{id=2,cid=2,did=3,sdid=3,price=110,date=new DateTime(2016, 02, 16),status="completed" },
            new Ride{id=3,cid=4,did=2,sdid=4,price=40,date=new DateTime(2016, 02, 16),status="completed" },
            new Ride{id=4,cid=2,did=2,sdid=5,price=70,date=new DateTime(2016, 02, 16),status="started" },
            new Ride{id=5,cid=3,did=3,sdid=2,price=100,date=new DateTime(2016, 02, 16),status="cancelled" },
             new Ride{id=6,cid=1,did=1,sdid=2,price=80,date=new DateTime(2016, 02, 16),status="pending" },
        };

        public static void MockGet()
        {
           

            LocationRepoMock.Setup(x => x.GetLocations()).Returns(locations);
            LocationRepoMock.Setup(x => x.GetLocationbyId(It.IsAny<int>())).Returns((int id) => locations.Where(x => x.id == id).FirstOrDefault());
        }
        public static void MockDelete()
        {

        }
        public static void MockPut()
        {

        }
        public static void MockPost()
        {

        }
    }
}
