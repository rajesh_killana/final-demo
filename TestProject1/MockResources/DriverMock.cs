﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using TrackX.Models.DB;
using TrackX.Repository.IRepository;

namespace TestProject1.MockResources
{
    public class DriverMock
    {
        public static readonly Mock<IDriverRepository> DriverRepoMock = new Mock<IDriverRepository>();

        public static List<Driver> drivers = new List<Driver>
        {
            new Driver
            {
                id = 1,
                name = "driver 1",
                phonenumber="989877978",
                place=4,
                vehiclenumber="xyz123"
            },
            new Driver
            {
                id = 2,
                name = "driver 2",
                phonenumber="9098090908",
                place=2,
                vehiclenumber="abcde"
            },
             new Driver
            {
                id = 3,
                name = "driver 3",
                phonenumber="9537210308",
                place=1,
                vehiclenumber="qwerty"
            },
        };
        public static void MockGet()
        {
            DriverRepoMock.Setup(x => x.GetNotifications(It.IsAny<int>())).Returns((int id) => LocationMock.rides.Where((r) => ( r.did == id)).Where((r)=>r.status=="pending").FirstOrDefault());
            DriverRepoMock.Setup(x => x.Getstatus(It.IsAny<int>())).Returns((int id) => LocationMock.rides.Where((r) => r.did == id).Where(r=> r.status=="pending").FirstOrDefault());
            DriverRepoMock.Setup(x => x.GetDriverbyID(It.IsAny<int>())).Returns((int id) => drivers.Where(x => x.id == id).FirstOrDefault());
        }
        public static void MockDelete()
        {
            
        }
        public static void MockPut()
        {
           
        }
        public static void MockPost()
        {
            
        }
    }
}
