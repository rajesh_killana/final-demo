﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using TrackX.Models.DB;
using TrackX.Repository.IRepository;

namespace TestProject1.MockResources
{
    public class CustomerMock
    {
        public static readonly Mock<ICustomerRespository> CustomerRepoMock = new Mock<ICustomerRespository>();

        public static List<Customer> customers = new List<Customer>
        {
            new Customer
            {
                id = 1,
                name = "customer 1",
                phonenumber="989877978"
            },
            new Customer
            {
                id = 2,
                name = "customer 2",
                phonenumber="9098090908"
            },
             new Customer
            {
                id = 3,
                name = "customer 3",
                phonenumber="9537210308"
            },
            new Customer
            {
                id = 4,
                name = "customer 4",
                phonenumber="9879870980"
            }
        };
        public static void MockGet()
        {
            CustomerRepoMock.Setup(x => x.Getstatus(It.IsAny<int>())).Returns((int id) => LocationMock.rides.Where((r) => r.cid == id).FirstOrDefault());
            CustomerRepoMock.Setup(x => x.GetCutomerbyId(It.IsAny<int>())).Returns((int id) => customers.Where(x => x.id == id).FirstOrDefault());
            CustomerRepoMock.Setup(x => x.Gethistory(It.IsAny<int>())).Returns((int id) => LocationMock.rides.Where((r) => r.cid == id));
        }
        public static void MockDelete()
        {

        }
        public static void MockPut()
        {

        }
        public static void MockPost()
        {

        }
    }
}
