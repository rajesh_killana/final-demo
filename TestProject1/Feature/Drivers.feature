﻿Feature: Drivers Features
Scenario: Get Drivers list nearby
	Given I am a client
	When I make GET Request '/drivers/place/3'
	Then response code must be '200'
	And response data must look like '[{"id":3,"name":"ram","phonenumber":"700098098","vehiclenumber":"ap12x457","place":2},{"id":5,"name":"ramesh","phonenumber":"8038832","vehiclenumber":"kshdsklj","place":4}]'

Scenario: Get driver with no notifications
	Given I am a client
	When I make GET Request '/drivers/11/notifications'
	Then response code must be '404'

Scenario: Get driver with valid notifications
	Given I am a client
	When I make GET Request '/drivers/1/notifications'
	Then response code must be '200'



Scenario: Cancel ride Request
		Given I am a client
		When I make a Request '/drivers/2/rideid/3/Cancel'
		Then response code must be '200'


Scenario: complete ride Request
		Given I am a client
		When I make a Request '/drivers/2/rideid/3/completed' 
		Then response code must be '200'

Scenario: Accept ride Request
		Given I am a client
		When I make a Request '/drivers/2/rideid/3/Cancel' 
		Then response code must be '200'

Scenario: Get Drivers status by id 
	Given I am a client
	When I make GET Request '/drivers/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"driver 1","phonenumber":"989877978","vehilce":"xyz123","placeid":0,"status":"in travel"}'