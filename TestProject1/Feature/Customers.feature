﻿Feature: Customers Resource

Scenario: Get Customers valid history by Id
	Given I am a client
	When I make GET Request '/customers/2/history'
	Then response code must be '200'
	And response data must look like '[{"driver":{"id":1,"name":"rahul","phonenumber":"989898989","vehiclenumber":"ap12ts123","placeid":0},"date":"1999-08-17T00:00:00","price":456,"source":"gandhinagar","destination":"mg road"}]'

Scenario: Get Customers history by invalid Id
	Given I am a client
	When I make GET Request '/customers/11/history'
	Then response code must be '200'
	And response data must look like '[]'

Scenario: Get Customer status by id 
	Given I am a client
	When I make GET Request '/customers/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"customer 1","phonenumber":"989877978","status":"in travel"}'

Scenario: post ride Request
		Given I am a client
		When I am making a post request to '/customers/requestRide' with the following Data '{"cid": 1,"did": 3,"sdid": 7,"price": 100,"date": "1999-08-17","status": "pending"}'
		Then response code must be '200'

