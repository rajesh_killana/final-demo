﻿Feature: Loactions Resource 
Scenario: Get Locations
	Given I am a client
	When I make GET Request '/locations'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"gandhinagar"},{"id":2,"name":"mg road"},{"id":3,"name":"besant road"},{"id":4,"name":"pvr mall"}]'