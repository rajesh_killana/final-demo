﻿using System;
using TechTalk.SpecFlow;
using TestProject1.StepFiles;
using Microsoft.Extensions.DependencyInjection;
using TestProject1.MockResources;

namespace TestProject1.Steps
{
    [Scope(Feature = "Drivers Features")]
    [Binding]
    public class DriverSteps : BaseSteps
    {
        
        public DriverSteps(CustomWebApplicationFactory factory)
           : base(factory.WithWebHostBuilder(builder =>
           {
               builder.ConfigureServices(services =>
               {
                   services.AddScoped(_ => DriverMock.DriverRepoMock.Object);
                   services.AddScoped(_ => LocationMock.LocationRepoMock.Object);
                   services.AddScoped(_ => CustomerMock.CustomerRepoMock.Object);
               });
           }))
        {
        }


        [BeforeScenario]
        public static void MocksGet()
        {
            DriverMock.MockGet();
        }
    }
}
