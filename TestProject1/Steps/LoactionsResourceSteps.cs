﻿using System;
using TechTalk.SpecFlow;
using TestProject1.StepFiles;
using Microsoft.Extensions.DependencyInjection;
using TestProject1.MockResources;

namespace TestProject1.Steps
{
    [Scope(Feature = "Loactions Resource")]
    [Binding]
    public class LocationResourceSteps : BaseSteps
    {
        public LocationResourceSteps(CustomWebApplicationFactory factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(_ => LocationMock.LocationRepoMock.Object);
                });
            }))
        {
        }
        [BeforeScenario]
        public static void MocksGet()
        {
            LocationMock.MockGet();
        }
    }
}
