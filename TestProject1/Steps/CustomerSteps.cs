﻿using System;
using TechTalk.SpecFlow;
using TestProject1.StepFiles;
using Microsoft.Extensions.DependencyInjection;
using TestProject1.MockResources;

namespace TestProject1.Steps
{

    [Scope(Feature = "Customers Resource")]
    [Binding]
    public class CustomerSteps : BaseSteps
    {
        public CustomerSteps(CustomWebApplicationFactory factory)
           : base(factory.WithWebHostBuilder(builder =>
           {
               builder.ConfigureServices(services =>
               {
                   services.AddScoped(_ => DriverMock.DriverRepoMock.Object);
                   services.AddScoped(_ => LocationMock.LocationRepoMock.Object);
                   services.AddScoped(_ => CustomerMock.CustomerRepoMock.Object);
               });
           }))
        {
        }

        [BeforeScenario]
        public static void MocksGet()
        {
            CustomerMock.MockGet();

        }
        [BeforeScenario]
        public static void MockPost()
        {

            CustomerMock.MockPost();
        }
        [BeforeScenario]
        public static void MockDelete()
        {

            CustomerMock.MockDelete();

        }
        [BeforeScenario]
        public static void MockPut()
        {

            CustomerMock.MockPut();

        }






    }
}
