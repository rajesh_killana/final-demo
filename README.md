list of endpoints

1) Get list of locations which will used in options

  /locations  -> httpget 
   sample data ->
             [{"id":1,"name":"gandhinagar"},{"id":2,"name":"mg road"},{"id":3,"name":"besant road"},{"id":4,"name":"pvr mall"},{"id":5,"name":"benz circle"},{"id":6,"name":"ntr circle"},{"id":7,"name":"railway station"},{"id":8,"name":"kr market"},{"id":9,"name":"bsnl office"},{"id":10,"name":"busstand"}]

2) get history of customer by id it may be null or list

     /customers/{id}/history -> httpget
      sample data -> [{"driver":{"id":2,"name":"lucky","phonenumber":"98987654","vehiclenumber":"ap2a123","place":1},"date":"1999-08-16T00:00:00","price":12,"source":"besant road","destination":"mg road","status":"completed"}]

3) post a request by customer to driver
       
       /customers/requestRide ->httppost
       a form is uploaded which consists 
       {  cid:int ,  did:int , sdid:int , price:int , status:string , date:datetime    }
         

4) get Customer details and status , which can be used to prevent to customer to place another ride
       
       /customers/{id} ->httpget
       sample data -> {"id":2,"name":"kamal","phonenumber":"9867054321","status":"available"}
        Here "available" represents that the user can able place a ride reqest

5) get list of drivers which are nearer to source and he is available
      /drivers/place/{sourceid} ->httpget
      sampledata ->  [{"id":3,"name":"ram","phonenumber":"700098098","vehiclenumber":"ap12x457","place":2},{"id":5,"name":"ramesh","phonenumber":"8038832","vehiclenumber":"kshdsklj","place":4}]

6) get the notification for driver on craeating a ride request it will handle by micro-services

   /drivers/{id}/notifications ->httpget
   sampledata ->  {"rideId":10,"customer":{"id":1,"name":"rajesh","phonenumber":"8247364226"},"date":"1999-08-17T00:00:00","price":100,"source":"pvr mall","destination":"besant road"}
         
7) get driver status and details  ,  which can be used to prevent to driver to accept another ride
     /drivers/{id} ->httpget
     sample data- > {"id":3,"name":"ram","phonenumber":"700098098","vehilce":"ap12x457","placeid":1,"status":"in travel"}

8)it is single query which can be used for change status of ride using rideid and driver id
 /drivers/{id}/rideid/{rid}/"completed" -> httppost

9)it is single query which can be used for change status of ride using rideid and driver id
 /drivers/{id}/rideid/{rid}/"Cancelled" -> httppost

10)it is single query which can be used for change status of ride using rideid and driver id
 /drivers/{id}/rideid/{rid}/"started" -> httppost