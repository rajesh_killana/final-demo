﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TrackX.Services;
using TrackX.Services.IServices;

namespace TrackX.Controllers
{
    [ApiController]
    [Route("drivers")]
    public class DriverController : ControllerBase
    {

        private readonly IDriverServices _driverServices;
        public DriverController(IDriverServices driverServices)
        {
            _driverServices = driverServices;
        }
       
        //get drivers list
        [HttpGet("place/{id}")]
        public IActionResult driverslist(int id)
        {
            return Ok(_driverServices.GetDrivers(id));
        }
        //notifications
        [HttpGet("{id}/notifications")]
        public IActionResult Getnotification(int id)
        {
            var notify = _driverServices.GetNotifications(id);
            return notify is null ? NotFound() : Ok(notify);
          
        }
        [Route("{id}")]
        [HttpGet]
        public IActionResult GetResult(int id)
        {
            var driver = _driverServices.GetDriver(id);
            return driver is null? NotFound():Ok(driver);
        }

        // accept ride
        [Route("{did}/rideId/{rid}/accepted")]
        [HttpPost]
        public IActionResult AcceptRide(int did,int rid)
        {
            _driverServices.AcceptRide(did, rid);
            return Ok();
        }

        //cancel ride
        [Route("{did}/rideId/{rid}/cancel")]
        [HttpPost]
        public IActionResult CancelRide(int did,int rid)
        {
            _driverServices.CancelRide(did, rid);
            return Ok();
        }

        //complete ride
        [Route("{did}/rideId/{rid}/completed")]
        [HttpPost]
        public IActionResult CompleteRide(int did,int rid)
        {
            _driverServices.CompleteRide(did, rid);
            return Ok();
        }

    }
}

