﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TrackX.Services;
using TrackX.Services.IServices;

namespace TrackX.Controllers
{
    [ApiController]
    [Route("locations")]
    public class LocationController : ControllerBase
    {

        private readonly ILocationService _locationService;
        public LocationController(ILocationService locationService)
        {
            _locationService = locationService;
        }
        // all location
        public IActionResult Locations()
        {
            return Ok(_locationService.GetLocations());
        }
    }
}

