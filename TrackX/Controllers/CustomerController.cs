﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TrackX.Models.Requestform;
using TrackX.Services;
using TrackX.Services.IServices;

namespace TrackX.Controllers
{
    [ApiController]
    [Route("customers")]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        // history
        [Route("{id}/history")]
        [HttpGet]
        public IActionResult History(int id)
        {
            var history = _customerService.Gethistory(id);
            return history is null ? NotFound() : Ok(history);
        }
        // user started the request
        [Route("requestRide")]
        [HttpPost]
        public IActionResult Post([FromBody]RequestRide Ride)
        {

            return Ok(_customerService.PostRide(Ride));
        }

        [Route("{id}")]
        [HttpGet]
        public IActionResult GetResult(int id)
        {
            return Ok(_customerService.GetCustomer(id));
        }

    }
}
