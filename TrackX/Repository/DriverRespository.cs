﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using TrackX.Models.DB;
using TrackX.Models.Response;
using TrackX.Repository.IRepository;

namespace TrackX.Repository
{
    public class DriverRespository : IDriverRepository
    {
        private readonly Connection _connection;
        private IDbConnection db;
        public DriverRespository(IOptions<Connection> connection)
        {
            _connection = connection.Value;
            db = new SqlConnection(_connection.DB);
        }
        
        public Driver GetDriverbyID(int id)
        {
            const string sql = @"select * from Drivers where id=@id";
            return db.QueryFirstOrDefault<Driver>(sql, new { id });
        }

        public IEnumerable<Driver> GetDrivers(int id)
        {
            const string sql = @"select d.* from drivers d join  source_destination sd
                                        on sd.destination=d.place and sd.distance<= 5 and sd.source=@id 
                                        join rides r on
                                        r.did = d.id and r.status<>'started'
                                        union
                                      select d.* from drivers d join  source_destination sd
                                       on sd.source=d.place and sd.distance<= 5 and sd.destination=@id
                                       join rides r on
                                       r.did = d.id and r.status<>'started'";
            return db.Query<Driver>(sql,new {@id=id});
        }
        public Ride GetNotifications(int id)
        {
            const string sql = @"select * from rides r where did=@id  and   status='pending'";
            return db.QueryFirstOrDefault<Ride>(sql, new {@id=id});
        }
        public void AcceptRide(int did, int rid)
        {
            const string sql = @"update rides  SET [status] = 'started' WHERE did = @did and id=@id;";
            db.Query(sql, new { @did = did, @id = rid });
        }
        public void CancelRide(int did, int rid)
        {
            const string sql = @"DELETE FROM rides WHERE WHERE did = @did and id=@id ;";
            db.Query(sql, new { @did = did, @id = rid });
        }

        public void CompleteRide(int did, int rid)
        {
            const string sql = @"update rides  SET [status] = 'completed' WHERE did = @did and id=@id;";
            db.Query(sql, new { @did = did, @id = rid });
        }
        public Ride Getstatus(int id)
        {
            const string sql = @"SELECT * FROM Rides WHERE dId = @id and status='pending'";
            return db.QueryFirstOrDefault<Ride>(sql, new { @id = id });
        }
    }

    }
