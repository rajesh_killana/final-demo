﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.DB;

namespace TrackX.Repository.IRepository
{
    public interface ILocationRepository
    {
        public IEnumerable<Location> GetLocations();
        public Location GetLocationbyId(int id);
        public Sourcedestination GetSourcedestination(int id);
    }
}
