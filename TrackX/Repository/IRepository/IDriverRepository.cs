﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.DB;
using TrackX.Models.Response;

namespace TrackX.Repository.IRepository
{
    public interface IDriverRepository
    {
        public Driver GetDriverbyID(int id);
        public IEnumerable<Driver> GetDrivers(int id);
        public Ride GetNotifications(int id);
        public void AcceptRide(int did, int rid);
        public void CancelRide(int did, int rid);

        public void CompleteRide(int did, int rid);
        public Ride Getstatus(int id);
    }
}
