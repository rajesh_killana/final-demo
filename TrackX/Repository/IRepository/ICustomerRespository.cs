﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.DB;

namespace TrackX.Repository.IRepository
{
    public interface ICustomerRespository
    {
        public Customer GetCutomerbyId(int id);
        public IEnumerable<Ride> Gethistory(int id);
        public int PostRide(Ride ride);
        public Ride Getstatus(int id);
    }
}
