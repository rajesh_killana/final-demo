﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using Microsoft.Extensions.Options;
using Microsoft.Data.SqlClient;
using TrackX.Models.Response;
using TrackX.Models.DB;
using TrackX.Repository.IRepository;

namespace TrackX.Repository
{
    public class CustomerRepository : ICustomerRespository
    {
        private readonly Connection _connection;
        private IDbConnection db;
        public CustomerRepository(IOptions<Connection> connection)
        {
            _connection = connection.Value;
            db = new SqlConnection(_connection.DB);
        }
        public IEnumerable<Ride> Gethistory(int id)
        {
            const string sql = @"SELECT * FROM Rides WHERE cId = @id";
            return db.Query<Ride>(sql, new {@id=id});
        }

        public Customer GetCutomerbyId(int id)
        {
            const string sql = @"SELECT * FROM Customers WHERE Id = @id";
            return db.QueryFirstOrDefault<Customer>(sql, new { @id = id });
        }

        public int PostRide(Ride ride)
        {
            const string sql = @"insert into rides (cid,did,sdid,price,date,status) values ( @cid,@did,@sdid,@price,@date,@status)";
            db.Query(sql, new { @cid = ride.cid, @did = ride.did, @sdid = ride.sdid, @price = ride.price, @date =ride.date,@status = "pending" });
            const string sql1 = @"select max(id) from rides";
            return db.QueryFirstOrDefault<int>(sql1);
        }

        public Ride Getstatus(int id)
        {
            const string sql = @"SELECT * FROM Rides WHERE cId = @id and status='pending'";
            return db.QueryFirstOrDefault<Ride>(sql, new { @id = id });
        }

    }
}
