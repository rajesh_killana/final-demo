﻿using System;
using System.Collections.Generic;
using System.Data;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using TrackX.Models.DB;
using TrackX.Repository.IRepository;

namespace TrackX.Repository
{
    public class LocationRepository : ILocationRepository
    {
        private readonly Connection _connection;
        private IDbConnection db;
        public LocationRepository(IOptions<Connection> connection)
        {
            _connection = connection.Value;
            db = new SqlConnection(_connection.DB);
        }
        public IEnumerable<Location> GetLocations()
        {
            const string sql = @"SELECT * FROM locations ";
            return db.Query<Location>(sql);
        }
        public Location GetLocationbyId(int id)
        {
            const string sql = @"select * from locations where id=@id";
            return db.QueryFirstOrDefault<Location>(sql, new { @id = id });
        }

        public Sourcedestination GetSourcedestination(int id)
        {
            const string sql = @"select * from source_destination where id=@id";
            return db.QueryFirstOrDefault<Sourcedestination>(sql, new { @id = id });

        }
    }
}
