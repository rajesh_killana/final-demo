﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.DB;
using TrackX.Models.Requestform;
using TrackX.Models.Response;
using TrackX.Repository;
using TrackX.Repository.IRepository;
using TrackX.Services.IServices;

namespace TrackX.Services
{
    public class CustomerServices : ICustomerService
    {
        private readonly IDriverRepository _driverRespository;
        private readonly ICustomerRespository _customerRespository;
        private readonly ILocationRepository _locationRepository;

        public CustomerServices(IDriverRepository driverRespository, ICustomerRespository customerRespository, ILocationRepository locationRepository)
        {
            _driverRespository = driverRespository;
            _customerRespository = customerRespository;
            _locationRepository = locationRepository;
        }

        public IEnumerable<RideHistory> Gethistory(int id)
        {
           var rides = _customerRespository.Gethistory(id);
            return rides.Select(m => new RideHistory
            {
                Driver = _driverRespository.GetDriverbyID(m.did),
                price =m.price,
                date= m.date,
                source = _locationRepository.GetLocationbyId(_locationRepository.GetSourcedestination(m.sdid).source).name,
                destination = _locationRepository.GetLocationbyId(_locationRepository.GetSourcedestination(m.sdid).destination).name,
               status =m.status
            }); ;

        }
        public int PostRide(RequestRide ride)
        {
            Ride rideobject = new Ride
            {
                cid = ride.cid,
                date = ride.date,
                did=ride.did,
                price=ride.price,
                sdid=ride.sdid
                
            };
            return _customerRespository.PostRide(rideobject);
        }
        public CustomerResponse GetCustomer(int id)
        {
            var customer=_customerRespository.GetCutomerbyId(id);
            CustomerResponse customerResponse = new CustomerResponse
            {
                id = customer.id,
                name= customer.name,
                phonenumber=customer.phonenumber,
                status = _customerRespository.Getstatus(id) is null ? "available" : "in travel"

            };
            return customerResponse;
        }

    }
}
