﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.Requestform;
using TrackX.Models.Response;

namespace TrackX.Services.IServices
{
    public interface ICustomerService
    {
        public IEnumerable<RideHistory> Gethistory(int id);
        public int PostRide(RequestRide ride);
        public CustomerResponse GetCustomer(int id);
    }
}
