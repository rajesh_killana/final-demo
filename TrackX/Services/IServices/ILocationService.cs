﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.DB;

namespace TrackX.Services.IServices
{
    public interface ILocationService
    {
          public IEnumerable<Location> GetLocations();
       
    }
}
