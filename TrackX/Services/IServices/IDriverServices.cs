﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.DB;
using TrackX.Models.Response;

namespace TrackX.Services.IServices
{
    public interface IDriverServices
    {
        public IEnumerable<Driver> GetDrivers(int id);
        public RideResponse GetNotifications(int id);

        public void AcceptRide(int did, int rid);
        public void CancelRide(int did, int rid);
        public void CompleteRide(int did, int rid);
        public DriverResponse GetDriver(int id);



    }
}
