﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.DB;
using TrackX.Models.Response;
using TrackX.Repository.IRepository;
using TrackX.Services.IServices;

namespace TrackX.Services
{
    
    public class DriverServices :IDriverServices
    {
        private readonly IDriverRepository _driverRespository;
        private readonly ICustomerRespository _customerRespository;
        private readonly ILocationRepository _locationRepository;
 
        public DriverServices(IDriverRepository driverRespository,ICustomerRespository customerRespository,ILocationRepository locationRepository)
        {
            _driverRespository = driverRespository;
            _customerRespository = customerRespository;
            _locationRepository = locationRepository;
        }
       
         public IEnumerable<Driver> GetDrivers(int id)
        {

            return _driverRespository.GetDrivers(id);
        }
        public RideResponse GetNotifications(int id)
        {
            Ride data = _driverRespository.GetNotifications(id);
            if (data == null)
            {
                return null;
            }
            var ride = new RideResponse
            {   
                rideId=data.id,
                date = data.date,
                customer = _customerRespository.GetCutomerbyId(data.cid),
                price = data.price,
                source = _locationRepository.GetLocationbyId(_locationRepository.GetSourcedestination(data.sdid).source).name,
            destination=_locationRepository.GetLocationbyId(_locationRepository.GetSourcedestination(data.sdid).destination).name
            };

            return ride;

        }
        public DriverResponse GetDriver(int id)
        {
            var driver = _driverRespository.GetDriverbyID(id);
            if (driver is null)
            {
                return null;
            }
            DriverResponse driverResponse = new DriverResponse
            {
                id = driver.id,
                name = driver.name,
                phonenumber = driver.phonenumber,
                vehilce=driver.vehiclenumber,
                status = _driverRespository.Getstatus(id) is null ? "available" : "in travel"

            };
            return driverResponse;
        }


        public void AcceptRide(int did, int rid)
        {
            _driverRespository.AcceptRide(did, rid);
        }
        public void CancelRide(int did, int rid)
        {
            _driverRespository.CancelRide(did, rid);
        }

        public void CompleteRide(int did, int rid)
        {
            _driverRespository.CompleteRide(did, rid);
        }
    }
}
