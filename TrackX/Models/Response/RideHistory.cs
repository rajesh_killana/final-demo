﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrackX.Models.DB;

namespace TrackX.Models.Response
{
    public class RideHistory
    {
        public Driver Driver { get; set; }
        public DateTime date { get; set; }
        public int price { get; set; }
        public string source { get; set; }
        public string destination { get; set; }
        public string status { get; set; }
    }
}
