﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackX.Models.Response
{
    public class CustomerResponse
    {
        public int id { get; set; }
        public string name { get; set; }
        public string phonenumber { get; set; }
        
        public string status { get; set; }
    }
}
