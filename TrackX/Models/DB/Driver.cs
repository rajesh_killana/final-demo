﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackX.Models.DB
{
    public class Driver
    {
        public int id { get; set; }
        public string name { get; set; }

        public string  phonenumber { get; set; }
        public string vehiclenumber { get; set; }

        public int place { get; set; }

    }
}
