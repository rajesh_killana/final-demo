﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackX.Models.DB
{
    public class Ride
    {
        public int id { get; set; }
        public int cid { get; set; }

        public int did { get; set; }
        public int sdid { get; set; }

        public int price { get; set; }

        public DateTime date { get; set; }
         
        public string status { get; set; }

    }
}
