﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackX.Models.DB
{
    public class Sourcedestination
    {
        public int id { get; set; }
        public int source { get; set; }
        public int destination { get; set; }
        public int distance { get; set; }

    }
}
