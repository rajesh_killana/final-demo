﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TrackX.Models.Requestform
{
    public class RequestRide
    {
        public int cid { set; get; }
        public int did { set; get; }

        public int sdid { set; get; }
        public int price { set; get; }
        public string status { set; get; }
        public DateTime date { set; get; }

    }
}
